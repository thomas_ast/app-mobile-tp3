package com.example.tp3;

import android.util.JsonReader;
import android.util.JsonToken;

import com.example.tp3.Match;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerMatch {

    private static final String TAG = JSONResponseHandlerMatch.class.getSimpleName();

    private Match match;


    public JSONResponseHandlerMatch(Match match) {
        this.match = match;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readMatchs(reader);
        } finally {
            reader.close();
        }
    }

    public void readMatchs(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("results")) {
                readArrayMatch(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayMatch(JsonReader reader) throws IOException {
        reader.beginArray();
        JsonToken check;
        int nb = 0; // only consider the first element of the array
        while (reader.hasNext()) {
            reader.beginObject();
            while (reader.hasNext()) {
                check = reader.peek();
                if(check != JsonToken.NULL) {
                    String name = reader.nextName();
                    if (nb == 0) {
                        if (name.equals("idEvent")) {
                            match.setId(reader.nextLong());
                        } else if (name.equals("strEvent")) {
                            match.setLabel(reader.nextString());
                        } else if (name.equals("strHomeTeam")) {
                            match.setHomeTeam(reader.nextString());
                        } else if (name.equals("strAwayTeam")) {
                            match.setAwayTeam(reader.nextString());
                        } else if (name.equals("intHomeScore")) {
                            check = reader.peek();
                            if (check != JsonToken.NULL) {
                                match.setHomeScore(reader.nextInt());
                            } else {
                                match.setHomeScore(0);
                            }
                        } else if (name.equals("intAwayScore")) {
                            check = reader.peek();
                            if (check != JsonToken.NULL) {
                                match.setAwayScore(reader.nextInt());
                            } else {
                                match.setAwayScore(0);
                            }
                        } else {
                            reader.skipValue();
                        }
                    } else {
                        reader.skipValue();
                    }
                }else{
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
